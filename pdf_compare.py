import pdftotext
import os
from tkinter import Tk     # from tkinter import Tk for Python 3.x
from tkinter.filedialog import askopenfilename

Tk().withdraw()

# Function to open file and add contents to one single string
def OpenAndReadFile(filename):
    #file = os.path.join(directory, filename)
    f = open(filename, "rb")
    pdf = pdftotext.PDF(f)

    # Iterate over all the pages
    text = "\n\n".join(pdf)
    return text


# Function that compares text strings from base and compare PDF
def ComparePDF(base_string, compare_string):
    if base_string == compare_string:
        return "Files are equal."
    else:
        return "Files have differences"

print("Open base file: ")
base_filename = askopenfilename()
base_file = OpenAndReadFile(base_filename)

print("Open compare file: ")
compare_filename = askopenfilename()
compare_file = OpenAndReadFile(base_filename)

print(ComparePDF(base_file, compare_file))